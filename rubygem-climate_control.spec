%global gem_name climate_control

Name: rubygem-%{gem_name}
Version: 0.0.3
Release: 1%{?dist}
Summary: Modify your ENV easily
Group: Development/Languages
License: MIT
URL: https://github.com/thoughtbot/climate_control
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(activesupport) >= 3.0
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(simplecov)
BuildRequires: rubygem(activesupport) >= 3.0
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
ClimateControl can be used to temporarily assign environment variables within a code block.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# remove unecessary gemspec
pushd .%{gem_instdir}
  rm %{gem_name}.gemspec
popd

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  rspec -Ilib spec
popd

%files
%dir %{gem_instdir}
%doc %{gem_instdir}/LICENSE.txt
%exclude %{gem_instdir}/.*
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/NEWS
%doc %{gem_instdir}/README.md
%{gem_instdir}/Gemfile
%{gem_instdir}/Rakefile
%{gem_instdir}/spec

%changelog
* Sat Jul 27 2013 ktdreyer@ktdreyer.com - 0.0.3-1
- Initial package, with gem2rpm 0.9.2
